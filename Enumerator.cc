#include "Enumerator.hh"
#include <cstdlib>

Enumerator::Enumerator(const std::string& filePath)
{
    inputStream.open(filePath.c_str());
    if(inputStream.fail())
    {
        std::cerr << "Cannot open file.";
        exit(1);
    }
    end = false;
}

Enumerator::~Enumerator()
{
    inputStream.close();
}

void Enumerator::init()
{
    inputStream >> cache;
    next();
}

void Enumerator::next()
{
    if(inputStream.fail())
    {
        end = true;
        return;
    }
    currentData.id = cache.id;
    currentData.pieces = 0;

    while(!inputStream.fail() && cache.id == currentData.id)
    {
        if(cache.type == "ponty" && cache.size >= 30)
        {
            currentData.pieces += 1;
        }
        inputStream >> cache;
    }
}
std::istream& operator>>(std::istream& in, Record& record)
{
    in >> record.id >> record.type >> record.size;

    return in;
}

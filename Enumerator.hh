#ifndef ENUMERATOR_HH_INCLUDED
#define ENUMERATOR_HH_INCLUDED

#include <iostream>
#include <fstream>

struct Result
{
    int id;
    int pieces;
};

struct Record
{
    int id;
    std::string type;
    int size;
};

std::istream& operator>>(std::istream& in, Record& record);

class Enumerator
{
private:

    std::ifstream inputStream;

    Record cache;
    Result currentData;

    bool end;
public:
    Enumerator(const std::string& filePath);
    ~Enumerator();

    void init();
    void next();
    Result current() const
    {
        return currentData;
    }
    bool isEnd() const
    {
        return end;
    }
};


#endif // ENUMERATOR_HH_INCLUDED

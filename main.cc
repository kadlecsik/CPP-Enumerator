#include <iostream>
#include "Enumerator.hh"

using namespace std;

int main()
{
    Enumerator e("input.txt");

    e.init();
    if(e.isEnd())
    {
        std::cout << "Empty file";
    }
    else
    {
        Result max = e.current();

        for(; !e.isEnd(); e.next())
        {
            if(e.current().pieces > max.pieces) max = e.current();
        }

        std::cout << "Max: " << max.id << " (" << max.pieces << " pieces)";
    }

}
